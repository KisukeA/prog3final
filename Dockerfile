FROM ubuntu:18.04
RUN mkdir -p /app

RUN apt update
RUN apt install default-jre -y
RUN apt install libssl-dev -y
RUN apt install libssl1.0.0 -y

COPY out/artifacts/unnamed/unnamed.jar /app/unnamed.jar
COPY vdf-cli /app
RUN pwd
RUN chmod 777 app/vdf-cli
RUN chmod +x app/vdf-cli
RUN mv app/vdf-cli /usr/bin/vdf-cli
WORKDIR /app
CMD ["java", "-jar", "unnamed.jar"]

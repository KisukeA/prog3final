public enum Protocol {
    PEER_DISCOVERY,
    CHAT_MESSAGE,
    PLAY,
    ADD,
    MOVE,
    LIST,
    CONNECT,
    RECONNECT,
    BLOCK,
    DOWNLOAD,
    SENT,
    DONEDOWNLOAD,
    DISTRIBUTE,
    REMOVE,
    VALIDATE,


    INVITE,
    BROADCAST,
    SERVER,
}



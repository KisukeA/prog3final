import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) throws IOException {
        String ipAddress="";
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            ipAddress= localHost.getHostAddress();
            System.out.println("Welcome to Peer-Tac-Toe");
            System.out.println("This game needs at least 3 online peers, as 1 needs to be the validator");
            System.out.println("type //list to see who is online, type //dm and the nickname you got from the list to text that peer");
            System.out.println("type //play to get in queue and wait for a game");
            System.out.println("Just type anything without these commands if you want to broadcast something to everyone");
            System.out.println("The board is like a 3x3 matrix with 13 being the top right square and 31 being the bottom left");
            System.out.println("When a game starts, enter your moves as two digit numbers like 11,12,...,33");
            System.out.println("Have fun!");
            System.out.println();
            System.out.println("IP Address: " + ipAddress);
        } catch (UnknownHostException e) {
            System.out.println("Unable to determine IP address: " + e.getMessage());
        }
        PeerA peer = new PeerA(ipAddress);
        peer.run();
    }
}

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

import com.google.gson.Gson;

public class PeerA implements Runnable{
    public static Lock lock = new ReentrantLock();
    public static int logCount = 0;
    public static ArrayList firstLog;
    public static ArrayList<String> waitingQ;
    public static Crypto crypto;
    public static KeyPair kp;
    public static List<Block> hashChain;
    public static int idCount = 0;//this is for the trusted only
    public static int myId;
    public Gson gson = new Gson();
    public static PeerManager pm;
    public static List<Socket> connectionList = new ArrayList<>();
    public static String address;
    private static final String trustedIp = "172.17.0.2";

    public PeerA(String ip) {
        address = ip;
        crypto = new Crypto(".");
        kp = crypto.getKeyPair();
        waitingQ = new ArrayList<>(0);
    }

    @Override
    public void run() {
        pm = new PeerManager();
        ServerPart sp = new ServerPart(pm,trustedIp.equals(address));
        Thread thread = new Thread(sp);
        thread.start();
        if(!trustedIp.equals(address)){
            try {
                Socket socket = new Socket(trustedIp,8000);
                pm.addPeer("0", socket);
                PrintWriter pw = new PrintWriter(socket.getOutputStream(),true);
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                Message msg = new Message(Protocol.PEER_DISCOVERY,address +" "+crypto.getPublicKey());
                pw.println(gson.toJson(msg));
                String res = br.readLine();
                myId = Integer.parseInt(res.split(" ")[0]);
                pm.addKey("0",res.split(" ")[1]);
                CommunicationHandler ch = new CommunicationHandler(socket,pm);
                new Thread(ch).start();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        ClientPart cp = new ClientPart(pm);
        Thread thread1 = new Thread(cp);
        thread1.start();
        HashChain hc = null;
        try {
            hc = new HashChain(trustedIp.equals(address),hashChain,pm,crypto);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Thread thread2 = new Thread(hc);
        thread2.start();
    }
}
class ServerPart implements Runnable{
    public Gson gson = new Gson();
    public PeerManager pm;
    private boolean trusted;
    public ServerPart(PeerManager peerManager, boolean b){
        this.pm = peerManager;
        this.trusted = b;
    }

    @Override
    public void run() {
        ServerSocket server = null;
        try {
            server = new ServerSocket(8000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        while(true){
            try {
                Socket peer = server.accept();
                if(!trusted){
                    String id = new BufferedReader(new InputStreamReader(peer.getInputStream())).readLine();
                    pm.addPeer(id.split(" ")[0],peer);
                    pm.addKey(id.split(" ")[0],id.split(" ")[1]);
                }
                Thread commHandler = new Thread(new CommunicationHandler(peer,pm));
                commHandler.start();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
class ClientPart implements Runnable {
    public static TicTacToe ttt;
    public static boolean goingFirst;
    public static int validator;
    public static String opp;
    public static boolean gameOn;
    public static Gson gson = new Gson();
    public PeerManager pm;
    public BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static ArrayList<String> gameLog;

    public ClientPart(PeerManager p){
        gameLog = new ArrayList<String>(0);
        this.pm = p;
        gameOn = false;
    }
    @Override
    public void run() {
        String output;
        boolean badOutput;
        while (true) {
            try {
                output = br.readLine();
                if(gameOn && output!=null){
                    if(goingFirst) {
                        //check if move valid
                        badOutput = true;
                        while (badOutput) {
                            while (!output.equals("11") && !output.equals("12") && !output.equals("13") &&
                                    !output.equals("21") && !output.equals("22") && !output.equals("23") &&
                                    !output.equals("31") && !output.equals("32") && !output.equals("33")) {
                                System.out.println("Wrong move, go again");
                                output = br.readLine();
                            }
                            if (containsOnlyNumbers(output) && ttt.isValid((Integer.parseInt(output) / 10) - 1, (Integer.parseInt(output) % 10) - 1)) {
                                Message send = new Message(Protocol.MOVE, output + " " + opp + " " + validator);
                                send(send);
                                goingFirst = !goingFirst;
                                badOutput = false;
                            } else {
                                System.out.println("Wrong move, go again");
                                output = br.readLine();
                            }
                        }
                    }
                    else{
                        System.out.println("not ur turn, wait");
                    }
                }
                else if (output != null && output.startsWith("//play")) {
                    Message send = new Message(Protocol.PLAY, String.valueOf(PeerA.myId));
                    send(send);
                } else if (output != null && output.startsWith("//list")) {
                    Message send = new Message(Protocol.LIST, "");
                    send(send);
                } else if (output != null && output.startsWith("//dm")) {
                    Message send = new Message(Protocol.CHAT_MESSAGE, output.substring(output.indexOf(" ") + 1));
                    send(send);
                } else if (output != null) {
                    Message send = new Message(Protocol.BROADCAST, output);
                    send(send);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
    public static boolean containsOnlyNumbers(String input) {
        String regex = "^[0-9]+$";
        Pattern pattern = Pattern.compile(regex);

        return pattern.matcher(input).matches();
    }

    public void send(Message msg) throws IOException {
        PrintWriter out;
        switch (msg.protocol) {
            case LIST:
                System.out.print("[ ");
                pm.connectionList.forEach((key, value) -> {
                    System.out.print(key + ", ");
                });
                System.out.println(" ]");
                break;
            case BROADCAST:
                System.out.println("------------^------------");
                pm.connectionList.forEach((key, value) -> {
                    try {
                        PrintWriter pw = new PrintWriter(value.getOutputStream(), true);
                        Message broadcast = new Message(Protocol.BROADCAST, PeerA.myId + ": " + msg.body);
                        pw.println(gson.toJson(broadcast));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                break;
            case CHAT_MESSAGE:
                String inputs[] = msg.body.split(" ");
                if (pm.connectionList.containsKey(inputs[0].toLowerCase())) {
                    Socket dm = pm.connectionList.get(inputs[0].toLowerCase());
                    out = new PrintWriter(dm.getOutputStream(), true);
                    //BufferedReader reader = new BufferedReader(new InputStreamReader(dm.getInputStream()));
                    Message chat = new Message(Protocol.CHAT_MESSAGE, PeerA.myId + ": " + msg.body.substring(msg.body.indexOf(" ") + 1));
                    out.println(gson.toJson(chat));
                    System.out.println("------------*------------");
                } else {
                    System.out.println("Sorry. Player " + inputs[0] + " was not found.");
                }
                break;
            case PLAY:
                System.out.println("You've been placed in waiting queue...");
                PeerA.waitingQ.add(String.valueOf(PeerA.myId));
                pm.connectionList.forEach((key, value) -> {
                    PrintWriter pw = null;
                    try {
                        pw = new PrintWriter(value.getOutputStream(), true);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    Message add = new Message(Protocol.ADD, String.valueOf(PeerA.myId));
                    pw.println(gson.toJson(add));
                    //System.out.println("<3");
                });
                //System.out.println(PeerA.waitingQ.toString());
                break;
            case MOVE:
                String[] s = msg.body.split(" ");
                ttt.move(Integer.parseInt(s[0])/10,Integer.parseInt(s[0])%10);
                ttt.gameState();
                System.out.println();
                //System.out.println("winner is: "+ttt.winner);
                Socket opp = pm.connectionList.get(s[1]);
                PrintWriter printWriter = new PrintWriter(opp.getOutputStream(),true);
                String now = Instant.now().toString();
                String signature;
                try {
                    signature = PeerA.crypto.sign(s[0]+now);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                gameLog.add(s[0]+" "+now+" "+signature);
                if (ttt.winner>=0){
                    endScreen(ttt.winner);
                }
                printWriter.println(gson.toJson(new Message(Protocol.MOVE,s[0]+" "+now+" "+signature+" "+PeerA.myId)));
                break;
        }
    }
    public static void endScreen(int winner) {
        gameOn = false;
      //  System.out.println("my validator is "+validator);
        Socket validatorSocket = PeerA.pm.connectionList.get(String.valueOf(validator));
        try {
            new PrintWriter(validatorSocket.getOutputStream(),true).println(gson.toJson(new Message(Protocol.VALIDATE,gson.toJson(gameLog))));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if(winner == 1){
            System.out.println("Player 1 won!");
        }
        else if(winner == 2){
            System.out.println("Player 2 won!");
        }
        else{
            System.out.println("It's a draw haha");
        }
    }
}
class CommunicationHandler implements Runnable{
    public PeerManager pm;
    public Socket socket;

    public PrintWriter out;
    public BufferedReader in;
    public Gson gson = new Gson();
    public Crypto crypto;
    public CommunicationHandler(Socket socket, PeerManager peerManager) throws IOException {
        this.socket = socket;
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new PrintWriter(socket.getOutputStream(),true);
        this.pm = peerManager;
        this.crypto = PeerA.crypto;
    }
    @Override
    public void run() {
        while(true){
            String input;
            Message msg;
            try {
                input = in.readLine();
                msg = gson.fromJson(input,Message.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            switch (msg.protocol){
                case CHAT_MESSAGE:
                    System.err.println(msg.body);
                    break;
                case BROADCAST:
                    System.err.println(msg.body);
                    break;
                case INVITE:
                    System.out.println("invited");
                    break;
                case ADD:
                    if(!PeerA.waitingQ.contains(Integer.parseInt(msg.body))){
                        PeerA.waitingQ.add(msg.body);
                        //System.out.println("peer "+msg.body +" added to waiting q");
                        //System.out.println(PeerA.waitingQ);
                    }
                    else{
                       System.out.println("already there");
                    }
                    break;
                case REMOVE:
                    //System.out.println("peer "+msg.body +" removed from waiting q");
                    PeerA.waitingQ.remove(msg.body);
                    //System.out.println(PeerA.waitingQ);
                    break;
                case PEER_DISCOVERY:
                    //System.out.println(pm.getAllPeers().keySet());
                    PeerA.idCount++;
                    out.println(PeerA.idCount + " "+PeerA.crypto.getPublicKey());
                    pm.getAllPeers().forEach((key,value) ->{
                        //System.out.println("aslooo");
                        PrintWriter pw;
                        try {
                            pw = new PrintWriter(value.getOutputStream(),true);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        Message message = new Message(Protocol.CONNECT,msg.body+" "+String.valueOf(PeerA.idCount));
                        pw.println(gson.toJson(message));
                    });
                    try {
                        pm.addPeer(String.valueOf(PeerA.idCount), socket);
                        pm.addKey(String.valueOf(PeerA.idCount),msg.body.split(" ")[1]);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case CONNECT:
                    try {
                        //System.out.println("ever?");
                        String[] as = msg.body.split(" ");
                        Socket peerSocket = new Socket(as[0],8000);
                        pm.addPeer(as[2],peerSocket);
                        pm.addKey(as[2],as[1]);
                        //System.out.println(pm.publicKeys.toString());
                        CommunicationHandler ch = new CommunicationHandler(peerSocket,pm);
                        new Thread(ch).start();
                        PrintWriter pw;
                        try {
                            pw = new PrintWriter(peerSocket.getOutputStream(),true);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        //Message message = new Message(Protocol.RECONNECT,PeerA.address+" "+PeerA.myId);
                        pw.println(String.valueOf(PeerA.myId)+" "+PeerA.crypto.getPublicKey());

                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case RECONNECT:
                    Socket peerSocket = null;
                    String[] as = msg.body.split(" ");
                    try {
                        peerSocket = new Socket(as[0],8000);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    try {
                        pm.addPeer(as[1],peerSocket);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    break;
                case DOWNLOAD:
                    HashChain.chain.forEach(block -> {
                        //System.out.println(HashChain.chain.size());
                            this.out.println(gson.toJson(new Message(Protocol.SENT,gson.toJson(block))));
                        }
                    );
                    this.out.println(gson.toJson(new Message(Protocol.DONEDOWNLOAD,"")));
                    //System.out.println("doneeeafdsfas");
                    break;
                case SENT:
                   // System.out.println("added a block");
                    HashChain.chain.add(gson.fromJson(msg.body,Block.class));
                    break;
                case DONEDOWNLOAD:
                    HashChain.donedownload.set(true);
                   // System.out.println("changed donedownload to "+HashChain.donedownload.get());
                    break;
                case DISTRIBUTE:
                    HashChain.chain.add(gson.fromJson(msg.body,Block.class));
                    break;
                case MOVE:
                    System.out.println("Your opponent made a move!");
                    ClientPart.ttt.move(Integer.parseInt(msg.body.split(" ")[0])/10,Integer.parseInt(msg.body.split(" ")[0])%10);
                    ClientPart.ttt.gameState();
                    System.out.println();
                    //System.out.println(msg.body);
                    //System.out.println(pm.publicKeys.get(msg.body.split(" ")[3]));
                    try {//here i tried to verify the signature with the public key which was signed by the private key of the opponent
                        //but for some reason i get an error/ i will paste the error in the report, and i don't know why that happens, im not sure
                        //so instead i just do the moves but i keep the signature in the logs and the validator doesn't verify it but rather checks
                        //if both of them are equal.
                        //System.out.println(crypto.verify(msg.body.split(" ")[0]+msg.body.split(" ")[1], msg.body.split(" ")[2],pm.publicKeys.get(msg.body.split(" ")[3])));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    ClientPart.gameLog.add(msg.body.split(" ")[0]+" "+msg.body.split(" ")[1]+" "+msg.body.split(" ")[2]);
                    if (ClientPart.ttt.winner>=0){
                        ClientPart.endScreen(ClientPart.ttt.winner);
                    }
                    ClientPart.goingFirst = !ClientPart.goingFirst;
                    /*System.out.println("winner is: "+ClientPart.ttt.winner);
                    System.out.println("was "+ClientPart.goingFirst);

                    System.out.println("now is "+ClientPart.goingFirst);
                    */break;
                case VALIDATE:
                    PeerA.lock.lock();
                    try {
                        PeerA.logCount++;
                        if(PeerA.logCount%2==1){
                            PeerA.firstLog = gson.fromJson(msg.body, ArrayList.class);
                            //System.out.println("is we here" +"  "+ PeerA.logCount);
                        }
                        else{
                            boolean areEqual = true;
                            ArrayList secondLog = gson.fromJson(msg.body, ArrayList.class);
                            //System.out.println("first "+PeerA.firstLog.toString());
                            //System.out.println("second "+secondLog.toString());
                            if (PeerA.firstLog.size() == secondLog.size()) {
                                for (int i = 0; i < PeerA.firstLog.size(); i++) {
                                    if (!PeerA.firstLog.get(i).equals(secondLog.get(i))) {
                                        areEqual = false;
                                        break;
                                    }
                                }
                            } else {
                                areEqual = false;
                            }
                            if (areEqual) {
                                System.out.println("Game is clear");
                            } else {
                                System.out.println("Somebody cheated");
                            }
                            PeerA.logCount = 0;
                        }

                    } finally {
                        PeerA.lock.unlock();
                    }
                    break;
            }

        }
    }
}
class HashChain implements Runnable{
    public static AtomicBoolean donedownload = new AtomicBoolean(false);
    private boolean trusted;
    public static List<Block> chain;
    private Block genesis;
    PeerManager manager;
    Crypto crypto;
    Gson gson = new Gson();
    public HashChain(boolean b, List<Block> chain, PeerManager pm,Crypto c) throws Exception {
        this.trusted = b;
        this.chain = chain;
        this.genesis = new Block(0,"0","abcdef",new ArrayList<>(0),PeerA.pm.connectionList.size()+1);
        this.manager = pm;
        this.crypto = c;

        genesis.signBlock(this.crypto);
    }
    @Override
    public void run() {
            if(trusted){
                chain = new ArrayList<Block>(0);
                chain.add(genesis);
            }
            else{
                try {
                    download();
                    //System.out.println("enter wait");
                    while(!donedownload.get()){
                        Thread.sleep(1000);
                        //System.out.println("why");
                    };
                   // System.out.println("exit wait");
                } catch (IOException | ClassNotFoundException | InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            while(true){
                String newHash = VDF.runVdf(chain.get(chain.size() - 1).hash);
                long seed = newHash.hashCode();
                Random random = new Random(seed);
                //System.out.println("sisisize is : "+(PeerA.pm.connectionList.size()+1));
                int lotteryWinner = random.nextInt(manager.connectionList.size()+1);
                //System.out.println();
                //System.out.println("winner is: "+lotteryWinner);
                Block newBlock = null;
                if(PeerA.myId == lotteryWinner){
                    //System.out.println("i create");
                    try {
                        newBlock = new Block(PeerA.myId,chain.get(chain.size()-1).hash,newHash,PeerA.waitingQ,PeerA.pm.connectionList.size()+1);
                        newBlock.signBlock(crypto);
                        //newBlock.generateMatchUps();

                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    Block finalNewBlock = newBlock;
                    PeerA.pm.connectionList.forEach((key, val) -> {
                        PrintWriter p = null;
                        try {
                            p = new PrintWriter(val.getOutputStream(),true);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        p.println(gson.toJson(new Message(Protocol.DISTRIBUTE,gson.toJson(finalNewBlock))));
                    });
                    chain.add(newBlock);
                }
                /*chain.forEach(block -> {
                    System.out.println("creator: "+block.creator);
                    System.out.println("hash: "+block.hash.substring(0,Math.min(block.hash.length(),15)));
                    System.out.println("previous: "+block.prevHash.substring(0,Math.min(block.prevHash.length(),15)));
                    System.out.println("sign: "+block.signature.substring(0,Math.min(block.signature.length(),15)));
                    System.out.println();
                });*/
                if(chain.get(chain.size()-1).wait.contains(String.valueOf(PeerA.myId))){

                    int myIndex = chain.get(chain.size()-1).wait.indexOf(String.valueOf(PeerA.myId));
                    int oppIndex;
                    if(myIndex%2==0){
                        oppIndex = myIndex+1;
                    }
                    else{
                        oppIndex = myIndex-1;
                    }
                    int validator = myIndex/2;
                    ClientPart.gameOn = true;
                    ClientPart.opp = chain.get(chain.size()-1).wait.get(oppIndex);
                    ClientPart.validator = chain.get(chain.size()-1).validators.get(validator);
                    ClientPart.goingFirst = (myIndex % 2) == 0;
                    ClientPart.ttt = new TicTacToe();
                    if (ClientPart.goingFirst) {
                        System.out.println("You are going first");
                    } else {
                        System.out.println("You are going second");
                    }
                    //PeerA.waitingQ.remove(chain.get(chain.size()-1).wait.get(oppIndex));
                    PeerA.waitingQ.remove(chain.get(chain.size()-1).wait.get(myIndex));
                    chain.get(chain.size()-1).wait.remove(String.valueOf(PeerA.myId));
                    //System.out.println(PeerA.waitingQ.toString());
                    PeerA.pm.connectionList.forEach((key, value) -> {
                        PrintWriter pw = null;
                        try {
                            pw = new PrintWriter(value.getOutputStream(), true);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        Message add = new Message(Protocol.REMOVE, String.valueOf(PeerA.myId));
                        pw.println(gson.toJson(add));
                        //System.out.println(PeerA.myId);
                    });
                }
            }
    }


    private void download() throws IOException, ClassNotFoundException {
        chain = new ArrayList<>(0);
        Socket down = manager.connectionList.get("0");
        PrintWriter pw = new PrintWriter(down.getOutputStream(),true);
        pw.println(gson.toJson(new Message(Protocol.DOWNLOAD,"")));
    }
}

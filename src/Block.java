import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Block implements Serializable {

    public int creator;
    public String hash;
    public String prevHash;
    public String signature;
    public ArrayList<String> wait = new ArrayList<>(0);
    public ArrayList<String> q;
    public ArrayList<Integer> validators;
    public int peers;
    public Block(int id, String prev, String current, ArrayList<String> waitList,int peers) throws Exception {
        this.peers = peers;
        this.q = waitList;
        this.creator = id;
        this.prevHash = prev;
        this.hash = current;
        this.wait = generateMatchUps();
    }
    private ArrayList<String> generateMatchUps(){
        ArrayList<String> ai = new ArrayList<String>(0);
        ArrayList<Integer> v = new ArrayList<Integer>(0);
        //System.out.println("q is "+q.toString());
        Random r = new Random(hash.hashCode());
        while(q.size()>=2){
            int c = r.nextInt(q.size());
            String first = q.get(c);
            q.remove(c);
            c = r.nextInt(q.size());
            String second = q.get(c);
            q.remove(c);
            ai.add(String.valueOf(first));
            ai.add(String.valueOf(second));
            int valid;
            do{
                valid = r.nextInt(peers);
                if(peers == 2){
                    valid = -1;
                }
            }while(valid == Integer.parseInt(first) || valid == Integer.parseInt(second));
            v.add(valid);
        }
        this.validators = v;
        //System.out.println("ai is "+ai.toString());
        return ai;
    }
    public void signBlock(Crypto cr) throws Exception {
        this.signature = cr.sign(hash+prevHash+creator+wait.toString()+validators.toString());
    }
}

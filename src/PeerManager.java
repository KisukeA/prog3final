import com.google.gson.Gson;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PeerManager {
    public Map<String,Socket> connectionList; // List to store peers
    public Map<String, String> publicKeys;
    //public ArrayList<Socket> connectionList;
    private ExecutorService executorService;
    public PeerManager() {
        //this.connectionList = new ArrayList<>();
        this.connectionList = new HashMap<>();
        //Map or HashMap??
        this.publicKeys = new HashMap<>();
        this.executorService = Executors.newCachedThreadPool();
    }

    public synchronized boolean addPeer(String key, Socket peer) throws IOException {
        /*if (!connectionList.contains(peer)) {
            connectionList.add(peer);
            /*executorService.submit(()->{
                try {
                    Socket connect = new Socket(peer.ip,P2PGameServer.PORT);

                    //peerList.add(connPeer);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }(ovde isto staj * /)

            });
            System.out.println("Peer " + peer + " added.");
            PrintWriter out = new PrintWriter(peer.getOutputStream(), true);
            out.println("ayee wsgood gang");
        } else {
            System.out.println("Peer " + peer + " already exists.");
        }*/
        if(!connectionList.containsKey(key)){
            connectionList.put(key.toLowerCase(),peer);
            System.out.println("Peer " + peer + " added.");
            //PrintWriter out = new PrintWriter(peer.getOutputStream(), true);
            //Message msg = new Message(Protocol.PEER_DISCOVERY,connectingPeer);
            //out.println(new Gson().toJson(msg));
            return true;
        }
        else{
            System.out.println("Peer " + peer + " already exists.");
            return false;
        }
    }
    public synchronized boolean addKey(String s, String k){
        if(!publicKeys.containsKey(k)){
            publicKeys.put(s,k);
            //System.out.println("key " + k + " added.");
            //PrintWriter out = new PrintWriter(peer.getOutputStream(), true);
            //Message msg = new Message(Protocol.PEER_DISCOVERY,connectingPeer);
            //out.println(new Gson().toJson(msg));
            return true;
        }
        else{
            System.out.println("key " + k + " already exists.");
            return false;
        }
    }
    /*public synchronized ArrayList<Socket> getAllPeers() {
        return connectionList; // Return a copy of the peerList
    }*/
    public synchronized Map<String,Socket> getAllPeers(){
        return connectionList;
    }
}

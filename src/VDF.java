import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class VDF {

    public static String runVdf(String challenge){
        ProcessBuilder pb = new ProcessBuilder();
        pb.command("vdf-cli",challenge,"60000");
        pb.redirectErrorStream(true);
        String proof;
        try {
         BufferedReader br=   new BufferedReader(new InputStreamReader(pb.start().getInputStream()));
         proof = br.readLine();
            //System.out.println("Proof: " + proof);

        } catch (IOException e) {
              throw new RuntimeException(e);
        }
        return proof;
    }

}
